# Golden Raspberry Awards - Teste TEXO IT

### Documentação de Referência
Projeto visa obter o produtor com maior intervalo entre dois prêmios consecutivos, e o que
obteve dois prêmios mais rápido

### Etapas
* Pré-Build
* Test
* Build
* Build Native
* Execução

### Pré-Build
Executa o empacotamento das clases

```
mvn clean package -Dmaven.test.skip=true
```

### Test
Executa os testes de Integração

```
mvn test
```

### Build
Gera o executável para distribuição

```
mvn install
```


### Build Native
Cria um executável otimizando com Graalvm par deploy em máquinas linux

```
$ ./mvnw spring-boot:build-image
```

Execução em um container docker:

```
$ docker run --rm -p 3333:3333 golden-raspberry-awards:0.0.1-SNAPSHOT
```


###Execução

### Estrutura de pastas 

```
- Pasta Raiz
	- Pasta input
		- movielist.csv
	- golden-raspberry-awards-0.0.1-SNAPSHOT.jar
```

### Instrução de Execução 

```
$ java -jar golden-raspberry-awards:0.0.1-SNAPSHOT
```

### API e documentação do Swagger
```
http://localhost:3333/api/v1/swagger-ui.html
```
