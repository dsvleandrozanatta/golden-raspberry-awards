package br.com.texoit.golden.raspberry.awards.config;

import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import br.com.texoit.golden.raspberry.awards.service.MoviesSaverService;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	private MoviesSaverService moviesSaverService;

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		log.info("Inicializando a leitura do arquivo csv");

		moviesSaverService.saveCSVFileToDatabase(Paths.get("input", "movielist.csv"));
	}
}