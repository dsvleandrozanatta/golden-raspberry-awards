package br.com.texoit.golden.raspberry.awards.config;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.boot.model.TypeContributions;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.CustomType;
import org.hibernate.usertype.UserType;

public class H2CustomDialect extends H2Dialect {

	public H2CustomDialect() {
		super();
		registerHibernateType(Types.ARRAY, ArrayType.HIBERNATE_TYPE_NAME);
	}

	@Override
	public void contributeTypes(TypeContributions typeContributions, ServiceRegistry serviceRegistry) {
		super.contributeTypes(typeContributions, serviceRegistry);

		typeContributions.contributeType(new CustomType(ArrayType.INSTANCE, new String[] { ArrayType.HIBERNATE_TYPE_NAME }));
	}
}

class ArrayType implements UserType {

	public static final String HIBERNATE_TYPE_NAME = "array_type";
	public static final ArrayType INSTANCE = new ArrayType();

	public int[] sqlTypes() {
		return new int[] { Types.ARRAY };
	}

	@SuppressWarnings("rawtypes")
	public Class<List> returnedClass() {
		return List.class;
	}

	public boolean equals(Object x, Object y) throws HibernateException {
		return x == null ? y == null : x.equals(y);
	}

	public int hashCode(Object x) throws HibernateException {
		return x == null ? 0 : x.hashCode();
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
			throws HibernateException, SQLException {

		if (names != null && names.length > 0 && rs != null && rs.getArray(names[0]) != null) {

			return Arrays.asList((Object[]) rs.getArray(names[0]).getArray());

		}

		return null;
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
			throws HibernateException, SQLException {
		throw new UnsupportedOperationException();
	}

	public Object deepCopy(Object value) throws HibernateException {
		throw new UnsupportedOperationException();
	}

	public boolean isMutable() {
		return false;
	}

	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}

	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}
}