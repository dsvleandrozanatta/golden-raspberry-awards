package br.com.texoit.golden.raspberry.awards.constants;

public class GoldenRaspberryArardsConstants {

	private GoldenRaspberryArardsConstants() {
	}

	public static final String PROTOCOLO_HTTP = "http";

	public static final String PATH_NOMINATED_PRODUCERS = "/nominatedProducers";
	public static final String PATH_AWARDS_INTERVAL = "/awardsInterval";
}
