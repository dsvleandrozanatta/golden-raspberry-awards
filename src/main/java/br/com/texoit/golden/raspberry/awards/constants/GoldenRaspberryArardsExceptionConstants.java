package br.com.texoit.golden.raspberry.awards.constants;

public class GoldenRaspberryArardsExceptionConstants {

	private GoldenRaspberryArardsExceptionConstants() {

	}

	public static final String SEM_VENCEDORES = "Não existem produtores vencedores com mais de dois prêmios na categoria Pior Filme.";
	public static final String SEM_INTERVALO = "Não foi encontrado o menor intervalo para os produtores informado.";
}
