package br.com.texoit.golden.raspberry.awards.controller;

import static br.com.texoit.golden.raspberry.awards.constants.GoldenRaspberryArardsConstants.PATH_AWARDS_INTERVAL;
import static br.com.texoit.golden.raspberry.awards.constants.GoldenRaspberryArardsConstants.PATH_NOMINATED_PRODUCERS;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.texoit.golden.raspberry.awards.dto.NominatedProducersDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Produtores nomeados", description = "API para recuperar dados de produtores nomeados")
@RequestMapping(value = PATH_NOMINATED_PRODUCERS, produces = { APPLICATION_JSON_VALUE })
public interface NominatedProducersController {

	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Intervalo de Vencedores encontrado", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = NominatedProducersDTO.class)) }),
			@ApiResponse(responseCode = "204", description = "Não foi encontrado intervalo de vencedores", content = @Content) })
	@Operation(summary = "Intervalo entre prêmios dos produtores", description = "Retorna o produtor com maior intervalo entre dois prêmios consecutivos, e o que obteve dois prêmios mais rápido", tags = {
			"Produtores nomeados" })
	@GetMapping(value = PATH_AWARDS_INTERVAL, produces = { APPLICATION_JSON_VALUE })
	public abstract ResponseEntity<NominatedProducersDTO> getAwardsInterval();
}