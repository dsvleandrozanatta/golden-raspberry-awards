package br.com.texoit.golden.raspberry.awards.controller.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import br.com.texoit.golden.raspberry.awards.controller.NominatedProducersController;
import br.com.texoit.golden.raspberry.awards.dto.NominatedProducersDTO;
import br.com.texoit.golden.raspberry.awards.service.NominatedProducersService;

@RestController
public class NominatedProducersControllerImpl implements NominatedProducersController {

	@Autowired
	private NominatedProducersService nominatedProducersService;

	@Override
	public ResponseEntity<NominatedProducersDTO> getAwardsInterval() {

		Optional<NominatedProducersDTO> optionalAwardsInterval = nominatedProducersService.getAwardsInterval();

		if (!optionalAwardsInterval.isPresent()) {

			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(optionalAwardsInterval.get());
	}

}