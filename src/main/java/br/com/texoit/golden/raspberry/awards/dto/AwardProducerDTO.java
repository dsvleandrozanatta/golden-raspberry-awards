package br.com.texoit.golden.raspberry.awards.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AwardProducerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String producer;

	private int interval;

	private int previousWin;

	private int followingWin;
}
