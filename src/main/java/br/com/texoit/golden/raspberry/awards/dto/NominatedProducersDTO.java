package br.com.texoit.golden.raspberry.awards.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NominatedProducersDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private AwardProducerDTO min;

	private AwardProducerDTO max;
}
