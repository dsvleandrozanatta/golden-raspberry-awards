package br.com.texoit.golden.raspberry.awards.exception;

public class TexoitException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TexoitException(String message) {
		super(message);
	}
}
