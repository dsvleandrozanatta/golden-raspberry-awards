package br.com.texoit.golden.raspberry.awards.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.texoit.golden.raspberry.awards.repository.domain.MovieDomain;

public interface MovieRepository extends CrudRepository<MovieDomain, Long> {

}
