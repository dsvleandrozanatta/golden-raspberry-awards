package br.com.texoit.golden.raspberry.awards.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.texoit.golden.raspberry.awards.repository.custom.ProducerRepositoryCustom;
import br.com.texoit.golden.raspberry.awards.repository.domain.ProducerDomain;

public interface ProducerRepository extends ProducerRepositoryCustom, CrudRepository<ProducerDomain, Long> {

}
