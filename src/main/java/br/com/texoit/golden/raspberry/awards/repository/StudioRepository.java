package br.com.texoit.golden.raspberry.awards.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.texoit.golden.raspberry.awards.repository.domain.StudioDomain;

public interface StudioRepository extends CrudRepository<StudioDomain, Long> {

}
