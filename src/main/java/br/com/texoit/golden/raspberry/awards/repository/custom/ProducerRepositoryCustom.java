package br.com.texoit.golden.raspberry.awards.repository.custom;

import java.util.List;

import br.com.texoit.golden.raspberry.awards.repository.projection.NominatedProducerProjection;

public interface ProducerRepositoryCustom {

	public List<NominatedProducerProjection> getWinnersNominatedProducers();
}
