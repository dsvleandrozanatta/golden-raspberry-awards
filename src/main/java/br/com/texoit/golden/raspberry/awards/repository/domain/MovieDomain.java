package br.com.texoit.golden.raspberry.awards.repository.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tb_movie")
@SequenceGenerator(name = "GEN_MOVIE", sequenceName = "GEN_MOVIE", allocationSize = 1)
public class MovieDomain implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_movie")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "GEN_MOVIE")
	private long id;

	@Column(name = "nr_year")
	private int year;

	@Column(name = "tx_title")
	private String title;

	@Column(name = "fl_winner")
	private boolean winner;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "tb_movieproducer", joinColumns = { @JoinColumn(name = "id_movie") }, inverseJoinColumns = {
			@JoinColumn(name = "id_producer") })
	private Set<ProducerDomain> producerDomains;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "tb_moviestudio", joinColumns = { @JoinColumn(name = "id_movie") }, inverseJoinColumns = { @JoinColumn(name = "id_studio") })
	private Set<StudioDomain> studioDomains;
}
