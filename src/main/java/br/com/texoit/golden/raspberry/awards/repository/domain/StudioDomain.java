package br.com.texoit.golden.raspberry.awards.repository.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tb_studio")
@SequenceGenerator(name = "GEN_STUDIO", sequenceName = "GEN_STUDIO", allocationSize = 1)
public class StudioDomain implements Serializable {

	private static final long serialVersionUID = 1L;

	public StudioDomain(String description) {
		this.description = description;
	}

	@Id
	@Column(name = "id_studio")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "GEN_STUDIO")
	private long id;

	@Column(name = "tx_description")
	private String description;

	@ManyToMany(mappedBy = "studioDomains")
	private Set<MovieDomain> movieDomains;
}
