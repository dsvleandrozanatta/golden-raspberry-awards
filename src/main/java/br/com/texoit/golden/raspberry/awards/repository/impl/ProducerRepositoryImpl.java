package br.com.texoit.golden.raspberry.awards.repository.impl;

import static br.com.texoit.golden.raspberry.awards.repository.domain.QMovieDomain.movieDomain;
import static br.com.texoit.golden.raspberry.awards.repository.domain.QProducerDomain.producerDomain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.SimpleTemplate;
import com.querydsl.jpa.sql.JPASQLQuery;
import com.querydsl.sql.H2Templates;
import com.querydsl.sql.RelationalPathBase;

import br.com.texoit.golden.raspberry.awards.repository.custom.ProducerRepositoryCustom;
import br.com.texoit.golden.raspberry.awards.repository.domain.ProducerDomain;
import br.com.texoit.golden.raspberry.awards.repository.projection.NominatedProducerProjection;

public class ProducerRepositoryImpl extends QuerydslRepositorySupport implements ProducerRepositoryCustom {

	public ProducerRepositoryImpl() {
		super(ProducerDomain.class);
	}

	@Override
	@PersistenceContext()
	public void setEntityManager(EntityManager entityManager) {
		super.setEntityManager(entityManager);
	}

	@Override
	@SuppressWarnings({ "rawtypes" })
	public List<NominatedProducerProjection> getWinnersNominatedProducers() {

		RelationalPathBase<Object> movieproducer = new RelationalPathBase<>(Object.class, "movieproducer", "public", "tb_movieproducer");
		NumberPath<Long> idmovie = Expressions.numberPath(Long.class, movieproducer, "id_movie");
		NumberPath<Long> idproducer = Expressions.numberPath(Long.class, movieproducer, "id_producer");

		SimpleTemplate<? extends List> simpleTemplate = Expressions.template(List.class, "ARRAY_AGG({0})", movieDomain.year);

		JPASQLQuery<Tuple> query = new JPASQLQuery<>(getEntityManager(), H2Templates.DEFAULT);

		return query.from(producerDomain).innerJoin(movieproducer).on(idproducer.eq(producerDomain.id)).innerJoin(movieDomain)
				.on(movieDomain.id.eq(idmovie)).where(movieDomain.winner.eq(true)).groupBy(producerDomain.description)
				.having(movieDomain.year.count().gt(1))
				.select(Projections.fields(NominatedProducerProjection.class, producerDomain.description, simpleTemplate.as("years"))).fetch();
	}
}
