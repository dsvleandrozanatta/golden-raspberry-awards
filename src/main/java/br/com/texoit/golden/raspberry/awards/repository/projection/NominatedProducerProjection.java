package br.com.texoit.golden.raspberry.awards.repository.projection;

import java.util.List;

import lombok.Data;

@Data
public class NominatedProducerProjection {

	private String description;

	private List<Integer> years;

}
