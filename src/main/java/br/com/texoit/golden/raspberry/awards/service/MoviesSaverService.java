package br.com.texoit.golden.raspberry.awards.service;

import java.nio.file.Path;

public interface MoviesSaverService {

	public void saveCSVFileToDatabase(Path csvFile);
}
