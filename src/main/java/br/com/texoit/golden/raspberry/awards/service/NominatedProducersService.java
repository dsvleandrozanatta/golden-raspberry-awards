package br.com.texoit.golden.raspberry.awards.service;

import java.util.Optional;

import br.com.texoit.golden.raspberry.awards.dto.NominatedProducersDTO;

public interface NominatedProducersService {

	public Optional<NominatedProducersDTO> getAwardsInterval();

}
