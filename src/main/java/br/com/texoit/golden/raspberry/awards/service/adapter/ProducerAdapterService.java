package br.com.texoit.golden.raspberry.awards.service.adapter;

import java.util.List;
import java.util.Set;

import br.com.texoit.golden.raspberry.awards.repository.domain.ProducerDomain;

public interface ProducerAdapterService {

	public Set<ProducerDomain> convertProducer(List<String> rawProducers);

	public List<String> getRawProducer(String producers);
}
