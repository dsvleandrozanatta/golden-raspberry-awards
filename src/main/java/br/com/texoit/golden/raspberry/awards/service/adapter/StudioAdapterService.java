package br.com.texoit.golden.raspberry.awards.service.adapter;

import java.util.List;
import java.util.Set;

import br.com.texoit.golden.raspberry.awards.repository.domain.StudioDomain;

public interface StudioAdapterService {

	public Set<StudioDomain> convertStudio(List<String> rawStudios);

	public List<String> getRawStudios(String studios);
}
