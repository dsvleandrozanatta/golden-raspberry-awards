package br.com.texoit.golden.raspberry.awards.service.adapter.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.texoit.golden.raspberry.awards.repository.domain.ProducerDomain;
import br.com.texoit.golden.raspberry.awards.service.adapter.ProducerAdapterService;

@Service
public class ProducerAdapterServiceImpl implements ProducerAdapterService {

	@Override
	public Set<ProducerDomain> convertProducer(List<String> rawProducers) {

		Set<ProducerDomain> producerDomains = new HashSet<>();

		rawProducers.forEach(producer -> generateProducer(producerDomains, getRawProducer(producer)));

		return producerDomains;
	}

	@Override
	public List<String> getRawProducer(String producer) {

		return Arrays.asList(producer.replace(" and ", ",").split(",")).stream().map(String::trim).collect(Collectors.toList());
	}

	private void generateProducer(Set<ProducerDomain> producerDomains, List<String> rawProducer) {

		producerDomains.addAll(rawProducer.stream().map(ProducerDomain::new).collect(Collectors.toSet()));
	}

}
