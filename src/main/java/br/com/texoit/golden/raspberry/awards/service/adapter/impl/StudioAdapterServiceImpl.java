package br.com.texoit.golden.raspberry.awards.service.adapter.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.texoit.golden.raspberry.awards.repository.domain.StudioDomain;
import br.com.texoit.golden.raspberry.awards.service.adapter.StudioAdapterService;

@Service
public class StudioAdapterServiceImpl implements StudioAdapterService {

	@Override
	public Set<StudioDomain> convertStudio(List<String> rawStudios) {
		Set<StudioDomain> studioDomains = new HashSet<>();

		rawStudios.forEach(producer -> generatStudios(studioDomains, getRawStudios(producer)));

		return studioDomains;
	}

	@Override
	public List<String> getRawStudios(String studios) {

		return Arrays.asList(studios.replace(" and ", ",").split(",")).stream().map(String::trim).collect(Collectors.toList());
	}

	private void generatStudios(Set<StudioDomain> studioDomains, List<String> rawStudios) {

		studioDomains.addAll(rawStudios.stream().map(StudioDomain::new).collect(Collectors.toSet()));
	}

}
