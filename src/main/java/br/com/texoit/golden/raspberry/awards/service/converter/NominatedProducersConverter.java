package br.com.texoit.golden.raspberry.awards.service.converter;

import java.util.List;

import br.com.texoit.golden.raspberry.awards.dto.NominatedProducersDTO;
import br.com.texoit.golden.raspberry.awards.repository.projection.NominatedProducerProjection;

public interface NominatedProducersConverter {

	public NominatedProducersDTO convert(List<NominatedProducerProjection> nominatedProducerProjections);
}
