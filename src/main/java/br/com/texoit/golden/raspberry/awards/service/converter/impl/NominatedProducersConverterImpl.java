package br.com.texoit.golden.raspberry.awards.service.converter.impl;

import static br.com.texoit.golden.raspberry.awards.constants.GoldenRaspberryArardsExceptionConstants.SEM_INTERVALO;
import static br.com.texoit.golden.raspberry.awards.constants.GoldenRaspberryArardsExceptionConstants.SEM_VENCEDORES;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.texoit.golden.raspberry.awards.dto.AwardProducerDTO;
import br.com.texoit.golden.raspberry.awards.dto.NominatedProducersDTO;
import br.com.texoit.golden.raspberry.awards.exception.TexoitException;
import br.com.texoit.golden.raspberry.awards.repository.projection.NominatedProducerProjection;
import br.com.texoit.golden.raspberry.awards.service.converter.NominatedProducersConverter;
import lombok.AllArgsConstructor;
import lombok.Data;

@Service
public class NominatedProducersConverterImpl implements NominatedProducersConverter {

	@Override
	public NominatedProducersDTO convert(List<NominatedProducerProjection> nominatedProducerProjections) {

		if (nominatedProducerProjections.isEmpty()) {

			throw new TexoitException(SEM_VENCEDORES);
		}

		return new NominatedProducersDTO(findMin(nominatedProducerProjections), findMax(nominatedProducerProjections));
	}

	private AwardProducerDTO findMin(List<NominatedProducerProjection> nominatedProducerProjections) {

		return nominatedProducerProjections.stream()
				.map(projection -> mappingToDTO(projection, Comparator.comparing(NominatedProducerCalculator::getInterval)))
				.sorted(Comparator.comparing(AwardProducerDTO::getInterval)).findFirst().orElseThrow(() -> new TexoitException(SEM_INTERVALO));
	}

	private AwardProducerDTO findMax(List<NominatedProducerProjection> nominatedProducerProjections) {

		return nominatedProducerProjections.stream()
				.map(projection -> mappingToDTO(projection, Comparator.comparing(NominatedProducerCalculator::getInterval).reversed()))
				.sorted(Comparator.comparing(AwardProducerDTO::getInterval).reversed()).findFirst()
				.orElseThrow(() -> new TexoitException(SEM_INTERVALO));
	}

	private AwardProducerDTO mappingToDTO(NominatedProducerProjection nominatedProducerProjection,
			Comparator<NominatedProducerCalculator> comparator) {

		NominatedProducerCalculator nominatedProducerCalculator = calculateInterval(nominatedProducerProjection.getYears(), comparator)
				.orElseThrow(() -> new TexoitException(SEM_INTERVALO));

		AwardProducerDTO awardProducerDTO = new AwardProducerDTO();
		awardProducerDTO.setProducer(nominatedProducerProjection.getDescription());
		awardProducerDTO.setFollowingWin(nominatedProducerCalculator.getFollowingWin());
		awardProducerDTO.setInterval(nominatedProducerCalculator.getInterval());
		awardProducerDTO.setPreviousWin(nominatedProducerCalculator.getPreviousWin());

		return awardProducerDTO;
	}

	private Optional<NominatedProducerCalculator> calculateInterval(List<Integer> years, Comparator<NominatedProducerCalculator> comparator) {
		List<NominatedProducerCalculator> calculosIntervalos = new ArrayList<>();

		years.sort(Comparator.naturalOrder());

		for (int posicao = 1; posicao < years.size(); posicao++) {

			calculosIntervalos
					.add(new NominatedProducerCalculator(years.get(posicao) - years.get(posicao - 1), years.get(posicao - 1), years.get(posicao)));
		}

		return calculosIntervalos.stream().sorted(comparator).findFirst();
	}

}

@Data
@AllArgsConstructor
class NominatedProducerCalculator {

	private int interval;

	private int previousWin;

	private int followingWin;
}
