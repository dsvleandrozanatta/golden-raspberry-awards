package br.com.texoit.golden.raspberry.awards.service.impl;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import br.com.texoit.golden.raspberry.awards.repository.MovieRepository;
import br.com.texoit.golden.raspberry.awards.repository.ProducerRepository;
import br.com.texoit.golden.raspberry.awards.repository.StudioRepository;
import br.com.texoit.golden.raspberry.awards.repository.domain.MovieDomain;
import br.com.texoit.golden.raspberry.awards.repository.domain.ProducerDomain;
import br.com.texoit.golden.raspberry.awards.repository.domain.StudioDomain;
import br.com.texoit.golden.raspberry.awards.service.MoviesSaverService;
import br.com.texoit.golden.raspberry.awards.service.adapter.ProducerAdapterService;
import br.com.texoit.golden.raspberry.awards.service.adapter.StudioAdapterService;
import br.com.texoit.golden.raspberry.awards.vo.MoviesVO;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MoviesSaverServiceImpl implements MoviesSaverService {

	@Autowired
	private ProducerAdapterService producerAtapterService;

	@Autowired
	private StudioAdapterService studioAdapterService;

	@Autowired
	private StudioRepository studioRepository;

	@Autowired
	private MovieRepository movieRepository;

	@Autowired
	private ProducerRepository producerRepository;

	@Override
	@Transactional
	public void saveCSVFileToDatabase(Path csvFile) {

		try (Reader reader = Files.newBufferedReader(csvFile)) {

			CsvToBean<MoviesVO> csvToBean = new CsvToBeanBuilder<MoviesVO>(reader).withType(MoviesVO.class).withSeparator(';').build();

			List<MoviesVO> moviesList = csvToBean.parse();

			Set<ProducerDomain> producers = producerAtapterService
					.convertProducer(moviesList.stream().map(MoviesVO::getProducers).distinct().collect(Collectors.toList()));

			Set<StudioDomain> studios = studioAdapterService
					.convertStudio(moviesList.stream().map(MoviesVO::getStudios).distinct().collect(Collectors.toList()));

			producers.forEach(producerRepository::save);
			studios.forEach(studioRepository::save);

			moviesList.forEach(movie -> saveMovie(movie, producers, studios));

			log.info("Persistido {} filmes com {} produtores e {} estúdios.", moviesList.size(), producers.size(), studios.size());

		} catch (IOException e) {

			log.error("Não foi possível efetuar a leitura do arquivo csv");
		}
	}

	private void saveMovie(MoviesVO movie, Set<ProducerDomain> producers, Set<StudioDomain> studios) {

		MovieDomain movieDomain = new MovieDomain();
		movieDomain.setYear(movie.getYear());
		movieDomain.setTitle(movie.getTitle());
		movieDomain.setWinner(movie.getWinner().equals("yes"));
		movieDomain.setProducerDomains(findProducers(movie.getProducers(), producers));
		movieDomain.setStudioDomains(findStudios(movie.getStudios(), studios));

		movieRepository.save(movieDomain);
	}

	private Set<StudioDomain> findStudios(String studios, Set<StudioDomain> studioDomains) {

		List<String> rawStudios = studioAdapterService.getRawStudios(studios);

		return studioDomains.stream().filter(studio -> rawStudios.contains(studio.getDescription())).collect(Collectors.toSet());
	}

	private Set<ProducerDomain> findProducers(String producers, Set<ProducerDomain> producerDomains) {

		List<String> rawProducers = producerAtapterService.getRawProducer(producers);

		return producerDomains.stream().filter(domain -> rawProducers.contains(domain.getDescription())).collect(Collectors.toSet());
	}

}
