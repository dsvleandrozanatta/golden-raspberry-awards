package br.com.texoit.golden.raspberry.awards.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.texoit.golden.raspberry.awards.dto.NominatedProducersDTO;
import br.com.texoit.golden.raspberry.awards.repository.ProducerRepository;
import br.com.texoit.golden.raspberry.awards.repository.projection.NominatedProducerProjection;
import br.com.texoit.golden.raspberry.awards.service.NominatedProducersService;
import br.com.texoit.golden.raspberry.awards.service.converter.NominatedProducersConverter;

@Service
public class NominatedProducersServiceImpl implements NominatedProducersService {

	@Autowired
	private ProducerRepository producerRepository;

	@Autowired
	private NominatedProducersConverter nominatedProducersConverter;

	@Override
	public Optional<NominatedProducersDTO> getAwardsInterval() {

		List<NominatedProducerProjection> nominatedProducerProjections = producerRepository.getWinnersNominatedProducers();

		if (nominatedProducerProjections.isEmpty()) {

			return Optional.empty();
		}

		return Optional.of(nominatedProducersConverter.convert(nominatedProducerProjections));
	}

}
