package br.com.texoit.golden.raspberry.awards.vo;

import lombok.Data;

@Data
public class MoviesVO {

	private int year;

	private String title;

	private String studios;

	private String producers;

	private String winner;
}
