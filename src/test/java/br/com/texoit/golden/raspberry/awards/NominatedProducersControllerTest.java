package br.com.texoit.golden.raspberry.awards;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class NominatedProducersControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void apiCall() throws Exception {

		mockMvc.perform(get("/nominatedProducers/awardsInterval").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.min.producer", is("Joel Silver"))).andExpect(jsonPath("$.min.interval", is(1)))
				.andExpect(jsonPath("$.max.producer", is("Matthew Vaughn"))).andExpect(jsonPath("$.max.interval", is(13)));
	}

}